package com.exemple.demo.restservice;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class RequestController {

    public final List<Request> cmdArray = new ArrayList<Request>();

    @Autowired
    private MeterRegistry meterRegistry;


    @PostMapping("/request")
    public Request createRequest(@RequestBody Request request) {
        meterRegistry.counter("request_counter", "url", request.getUrlRequested()).increment();
        meterRegistry.counter("country_counter", "country", request.getCustomerCountry()).increment();
        request.setId(UUID.randomUUID().toString());
        cmdArray.add(request);
        return request;
    }
}
