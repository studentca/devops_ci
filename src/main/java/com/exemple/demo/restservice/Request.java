package com.exemple.demo.restservice;

import lombok.Data;

@Data
public class Request {
    private String id;
    private String urlRequested;
    private String customerName;
    private String customerCountry;
}
