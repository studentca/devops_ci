function fn() {
   var env = karate.env; // get java system property 'karate.env'
   karate.log('karate.env system property was:', env);
   if (!env) {
       env = 'dev'; // a custom 'intelligent' default
   }
   var config = { // base config JSON
       baseUrl: 'http://localhost:8445/api'
   };
   if (env === 'stage') {
       // override only those that need to be
       config.baseUrl = 'https://stage-host/api';
   } else if (env === 'e2e') {
       config.baseUrl = 'https://e2e-host/api';
   }
   // don't waste time waiting for a connection or if servers don't respond within 5 seconds
   karate.configure('connectTimeout', 5000);
   karate.configure('readTimeout', 5000);
   return config;
}